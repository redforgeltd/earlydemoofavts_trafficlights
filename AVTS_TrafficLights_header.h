

#include <18F2620.h>             // Written by CCS
#device *=16                     // Use 16-bit pointer to make full use of all
                                 // RAM locations (redundant for 18F252)
#device ADC=10                   // Select 10-bit ADC (0 to 1023)


#FUSES INTRC_IO


// 4MHz clock (internal clock of MCU=1MHz)
#use     delay(clock=4000000, restart_wdt)



typedef unsigned int8   U8;
typedef signed int8     S8;
typedef unsigned int16  U16;
typedef signed int16    S16;
typedef unsigned int32  U32;
typedef signed int32    S32;
typedef int1            bool;

// Timer0, Timer1, Timer3 values
#define T10ms16bit            64286  // 65536 - (0.010/(4/(4000000/8))
#define T25ms16bit            62411  // 65536 - (0.025/(4/(4000000/8))
#define T100ms16bit           53036  // 65536 - (0.100/(4/(4000000/8))
#define T250ms16bit           34286  // 65536 - (0.250/(4/(4000000/8))
#define T500ms16bit            3036  // 65536 - (0.500/(4/(4000000/8))


