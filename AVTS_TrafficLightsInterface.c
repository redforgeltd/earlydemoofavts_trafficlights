/*****************************************************************************
      Red Forge Ltd
      9, Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

 
      
**********************************************************************************************************************************/

#include "AVTS_TrafficLights_header.h"           // Header file contains constants and include file

   

 
#use rs232(baud=9600,  xmit=PIN_C6, rcv=PIN_C7, stream=VTS_COMMS)
 




U8 kb_ret;






U8 tris_e_image=0b00011000;// So RE2  can be toggled later as required for AUX2 direction

U8 i;







#case

#include <stdlib.h>




#separate
void setup_ports(void)
{

	set_tris_a(0b11111000);
// Ensure traffic lights OFF before tris to outputs.
	output_b(0);
    set_tris_b(0b00000000);
    set_tris_c(0b10000000);  //
    set_tris_e(tris_e_image);  // PIN E2 = AUX2(RE2) not set here, other i/o is
    port_b_pullups( TRUE);
 }


#define BUFFER_SIZE 128					
#define bkbhit (next_in!=next_out) 

#define AVTS_BUFFER_SIZE    30


#define	FRONT_LED	PIN_A4
#define	REAR_LED		PIN_A2
#define	GROSS_LED	PIN_A3

#define	FRONT_LED_ON	1
#define	REAR_LED_ON		1
#define	GROSS_LED_ON	1


// First board
// #define 	GREEN_LAMP	PIN_B0
// #define	AMBER_LAMP	PIN_B1
// #define	RED_LAMP		PIN_B2

// 20mm LED box
#define 	GREEN_LAMP	PIN_B3
#define	AMBER_LAMP	PIN_B4
#define	RED_LAMP		PIN_B5

// First board
#define	RTS_PIN		PIN_C4


// 20mm LED box
//#define	RTS_PIN		PIN_C6



#define	FRONT	0
#define	REAR	1
#define	GROSS	2

#define 	GREEN 		0
#define 	AMBER 		1
#define 	RED			2
#define	ALL_OFF		3

#define	RED_PERCENT		97
#define	AMBER_PERCENT	89
#define	HYSTER_PERCENT	3

// Offset in buffer[] - from 0 - AVTS pkt to front, rear and gross% values
#define	FRONT_OFFSET	2
#define	REAR_OFFSET		8
#define	GROSS_OFFSET	14


//use T0 to detect comms fial and to flash led(s)
#define T500msValue            15536  



/***************************************************************************************/
BYTE buffer[BUFFER_SIZE]; 
BYTE next_in = 0; 
BYTE next_out = 0; 


U8 u8InterPktTimer;
U8 u8AmberFlashCount, u8FlashCount;
bool bCommsOK, bChannelInAmber;
U8 FrontState, RearState, GrossState;


U8 CheckChannel( U8 FrontRearOrGross)
{
static U8 u8FrontState=GREEN;
static U8 u8RearState=GREEN;
static U8 u8GrossState=GREEN;

U8 u8OffsetToPercentage;
U8 u8CurrentState;

	switch(FrontRearOrGross)
	{
		case FRONT:
		u8OffsetToPercentage=FRONT_OFFSET;	
		u8CurrentState=u8FrontState;
		break;
		
		case REAR:
		u8OffsetToPercentage=REAR_OFFSET;		
		u8CurrentState=u8RearState;
		break;
		
		case GROSS:
		u8OffsetToPercentage=GROSS_OFFSET;		
		u8CurrentState=u8GrossState;
		break;
	};



// Get front/rear or gross % from buffer[]
	buffer[u8OffsetToPercentage+3] = '\0';// overwrite comma following field with NULL
	
	U8 CurrentPercent = atoi(&buffer[u8OffsetToPercentage]);
	switch(u8CurrentState)
	{
		case GREEN:
		if(CurrentPercent>RED_PERCENT)
			u8CurrentState = RED;
		else if  (CurrentPercent>AMBER_PERCENT)
			u8CurrentState = AMBER;		 
		break;
		
		case AMBER:
		if(CurrentPercent > RED_PERCENT)
			u8CurrentState = RED;
		else if (CurrentPercent < (AMBER_PERCENT-HYSTER_PERCENT))
			u8CurrentState=GREEN;	
		break;
		
		case RED:
		if (CurrentPercent < (AMBER_PERCENT-HYSTER_PERCENT))
			u8CurrentState=GREEN;
		else if (CurrentPercent < (RED_PERCENT-HYSTER_PERCENT))
			u8CurrentState = AMBER;
		break;
	
	};
	if	(FrontRearOrGross==FRONT)	
		u8FrontState = u8CurrentState;
		
	else if	(FrontRearOrGross==REAR)
		u8RearState = u8CurrentState;
	else
		u8GrossState = u8CurrentState;

	return u8CurrentState;
}

void AllLEDs_out()
{
output_bit(FRONT_LED, !FRONT_LED_ON);
output_bit(REAR_LED, !REAR_LED_ON);
output_bit(GROSS_LED, !GROSS_LED_ON);
}

void AssertLEDs( U8 FrontState, U8 RearState, U8 GrossState)
{
	switch (FrontState)
	{
		case RED:
		output_bit(FRONT_LED, FRONT_LED_ON);
		break;
	
		case AMBER:
		//////////////output_toggle(FRONT_LED);
		break;
		
		case GREEN:
		output_bit(FRONT_LED, !FRONT_LED_ON);
		break;
		
	};

	switch (RearState)
	{
		case RED:
		output_bit(REAR_LED, REAR_LED_ON);
		break;

		case AMBER:
		/////////////output_toggle(REAR_LED);
		break;
		
		case GREEN:
		output_bit(REAR_LED, !REAR_LED_ON);
		break;

	};

	switch (GrossState)
	{
		case RED:
		output_bit(GROSS_LED, GROSS_LED_ON);
		break;

		case AMBER:
		//////////////output_toggle(GROSS_LED);
		break;
		
		case GREEN:
		output_bit(GROSS_LED, !GROSS_LED_ON);
		break;

	
	};
	
	
	
}

void SetTrafficLights( U8 u8Colour)
{
	if  (u8Colour==GREEN)
	{
		output_bit(GREEN_LAMP, 1);
		output_bit(AMBER_LAMP, 0);
		output_bit(RED_LAMP, 0);
	}
	else if (u8Colour==AMBER)
	{
		output_bit(GREEN_LAMP, 0);
		output_bit(AMBER_LAMP, 1);
		output_bit(RED_LAMP, 0);
	}
	else if (u8Colour==RED)
	{
		output_bit(GREEN_LAMP, 0);
		output_bit(AMBER_LAMP, 0);
		output_bit(RED_LAMP, 1);
	}
	else //turn all off
	{
		output_bit(GREEN_LAMP, 0);
		output_bit(AMBER_LAMP, 0);
		output_bit(RED_LAMP, 0);
	}	
}

void AssertTrafficLights(U8 FrontState, U8 RearState, U8 GrossState)
{

	if  ( (FrontState==RED) ||  (RearState==RED) || (GrossState==RED ))
		SetTrafficLights( RED);
	else if ( (FrontState==AMBER) ||  (RearState==AMBER) || (GrossState==AMBER ))
		SetTrafficLights( AMBER);
	else
		SetTrafficLights( GREEN);

}

void ToggleAmberChannel()
{
	if  (FrontState==AMBER)
		output_toggle(FRONT_LED);
	if  (RearState==AMBER)
		output_toggle(REAR_LED);
	if  (GrossState==AMBER)
		output_toggle(GROSS_LED);
		
}

void  ToggleAllLamps()
{
	output_toggle(GREEN_LAMP);
	output_toggle(AMBER_LAMP);
	output_toggle(RED_LAMP);	
}

/***************************************************************************************/
//Add RX   data to the circular buffer 
#int_rda 
void serial_isr() 
{ 
   int t; 

U8 Rxchar;
    Rxchar=getc(); 
    buffer[next_in] = Rxchar;
   if  (Rxchar==0x0d)// terminating <CR> on XVTS packet - only posible occurrence (and position)
   {
 // for packet to be valid, the CR must be at  AVTS_BUFFER_SIZE-1, else discard data
      if  (next_in==(AVTS_BUFFER_SIZE-1) )// in correct posn, check the CSM
      {
          U8 checksum=0, i;
/*         for(i=0; i<next_in; i++)
         {
            checksum ^= buffer[i];
         }
         sprintf( strTemp, "%02X", checksum);
         if  ( (strTemp[0]!=buffer[next_in-2]) || (strTemp[1]!=buffer[next_in-1]))// csm OK
            next_in=0;// start filling buffer again          */
      }
      else
        next_in=0;// start filling buffer again
   }
   else
	{
	   t=next_in; 
	   next_in=(next_in+1);  // these 2 lines instead of modulo 
	   if (next_in==BUFFER_SIZE) next_in=0; // end of buffer
	   if(next_in==next_out)  next_in=t;   
	}        
} 



 #int_timer0
    void intTimer0()
	 {

      set_timer0( T500msValue + get_timer0() ); 
		if  (u8InterPktTimer>0)
			u8InterPktTimer--;// tested for 0 in packet getting loop.
		if  (bCommsOK == FALSE)// in comms fail, flash all three lamps
		{
			if  (u8FlashCount>0)
			{
				u8FlashCount--;
				if   (u8FlashCount==0)
				{
					u8FlashCount=5;//5X0.1 gives 2 flashes per sec.
					ToggleAllLamps();
				}
			}
		}
		
		if  (u8AmberFlashCount>0)// need to flash channel led to show which one (or more) in amber state.
			u8AmberFlashCount--;
		else// Amber flash timed out
		{
			if  (bChannelInAmber)// one or more channel in amber state, flash channel's led
			{
				ToggleAmberChannel();
				u8AmberFlashCount=5; //2Hz flash rate
			}
		}

    }


U16 u16GreenON =2500;
U16 u16AmberON = 1250;
U16 u16RedON = 2000;

void main(void)
{







   setup_ports();          // Configure MCU ports as inputs or outputs
	

	
	while(0)// demo of lamps
	{
		SetTrafficLights(GREEN);
		delay_ms(u16GreenON);
		SetTrafficLights(AMBER);
		delay_ms(u16AmberON);
		SetTrafficLights(RED);
		delay_ms(u16RedON);
	}
	
	setup_timer_0( RTCC_INTERNAL | RTCC_DIV_2 );
	set_timer0( T500msValue ); 
	
	enable_interrupts( INT_TIMER0 );


   clear_interrupt(INT_TIMER0);
   enable_interrupts(GLOBAL);

   // The next lines of code are ALL needed - otherwise an immediate RXIRQ occurs
   // CCS made aware...March 2006
   delay_ms(1);
   do
   {
      restart_wdt();                // Reset watchdog to 2.3s +/- 50%
      kb_ret = kbhit();
      if  (kb_ret !=0)
         fgetc(VTS_COMMS);// flush rx fifo
   }while(kb_ret !=0);

    enable_interrupts(INT_RDA);

 
 
 
 
 
 // Set RTS active to send pkts
 output_bit(RTS_PIN, 0);
    
// Code to handle AVTS 
    while(1)
    {

// Wait for complete AVTS packet
        if  (next_in==(AVTS_BUFFER_SIZE-1))
        {
		  // not in comms fail
					u8InterPktTimer=5;
					bCommsOK=TRUE;
					FrontState = CheckChannel(FRONT);// returns G, A or R
					RearState = CheckChannel(REAR);// returns G, A or R
					GrossState = CheckChannel(GROSS);// returns G, A or R
					bChannelInAmber=FALSE;// assume no channel in amber state
					if  ( (FrontState==AMBER) || (RearState==AMBER) ||(GrossState==AMBER))
						bChannelInAmber=TRUE;
					AssertLEDs( FrontState, RearState, GrossState);
					AssertTrafficLights( FrontState, RearState, GrossState);
					next_in=0;		
			}
			else // not RX packet, check for interpacket timeout
			{
				if  ((u8InterPktTimer==0)&& bCommsOK)// timed out
				{
					bCommsOK=FALSE;
					u8FlashCount=10;// counts between toggling traffic lights
					SetTrafficLights(ALL_OFF);
					AllLEDs_out();
				}
			}
    
    }
          
}




