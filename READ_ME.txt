
Code is for traffic light system driven by AVTS data.
Originally developed for BryLynx.
The combination MUST operate in AVTS mode, VTS continuous and with damping OFF/light

A folder AVTS_TrafficLights holds the code for the traffic light box.
The switching levels and hysteresis are defined in the source.
It operates on payload values.

Off this is a folder DisplayUnitForTrafficLights/Source with the following mods

	A2XL OmniweighDisplayMain.c is the only source affected.
	Code change forces transmit AVTS pkt whenever "PrepareAndSendVTSpercentPkt" is called and (CONTINUOUS and RTS is low )
	This consists of the following addition
		if ((CDATAimage.VTSsetting == LOGGING_CONT) && !input(VTS_REQUEST))
		VTScommsTimer=1;
		
In Version.h because this is a functional change/extension #define FUNCTION 2 is changed to 3.